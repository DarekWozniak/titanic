import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    """
    Get amount of missing values with their medians.
    Update the 'Age' column of the DataFrame
    """
    df = get_titatic_dataframe()
    titles = ["Mr.", "Mrs.", "Miss."]
    medians = []
    missing = []
    for title in titles:
        missing.append(df[df['Name'].str.contains(title)]['Age'].notnull().sum())
        medians.append(df[df['Name'].str.contains(title)]['Age'].median())

    return [(title, miss, round(med)) for (title, miss, med) in zip(titles, missing, medians)]
